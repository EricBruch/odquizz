#from datetime import datetime

#from werkzeug.security import generate_password_hash, check_password_hash
from flask import Flask, request, url_for, redirect, g, session, flash, \
    abort, render_template, json, make_response

#from flask.ext.oauth import OAuth
from flask_sqlalchemy import SQLAlchemy

from flask_migrate import Migrate

from flask_login import LoginManager, current_user, login_user, logout_user, login_required #UserMixin,

from parse_table import csvclean_service

from config import Config
import codecs

#from settings import CONSUMER_KEY, CONSUMER_SECRET

table_url = None
#### init odquizz
app = Flask(__name__)
app.config.from_object(Config)  #load configuration secret key & database
db = SQLAlchemy(app)
migrate = Migrate(app, db)
from models import *
db.create_all() # initalize database



login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
#### init odquizz end


#oauth = OAuth()
#facebook = oauth.remote_app('facebook',
#    base_url='https://graph.facebook.com/',
#    request_token_url=None,
#    access_token_url='/oauth/access_token',
#    authorize_url='https://www.facebook.com/dialog/oauth',
#    consumer_key=CONSUMER_KEY,
#    consumer_secret=CONSUMER_SECRET,
#    request_token_params={'scope': 'email'}
#)


@app.before_request
def check_user_status():
    g.user = None
    if 'user_id' in session:
        g.user = User.query.get(session['user_id'])


def generate_table(table_url, sample_size):#, startLineCSV
    table = csvclean_service(table_url, sample_size)#, startLineCSV
    # run with default csv url
    # table = csvclean_service()
    header = json.dumps(table.header_line)
    rows = json.dumps(table.sample[1:])
    #print rows
    return (header, rows)


@app.route('/', methods=['GET', 'POST'])    #adds new questions
@login_required
def new_question():
    # sample size is size of data entries
    rows, header, visible_rows, sample_size, table_url = None, None, None, 10000000000, None
    if request.method == 'POST':
        table_url = request.form['url']
        #startLineCSV = 0    #defines row were csv starts loading
        header, rows = generate_table(table_url, sample_size)#, startLineCSV
        visible_rows = 20   #rows visible on screen
    return render_template('new_question.html', rows=rows, header=header, clickable=True,
                           table_url=table_url,visible_rows=visible_rows)


@app.route('/about', methods=['GET'])
def about():
    return render_template('about.html')


@app.route('/contact', methods=['GET'])
def contact():
    return render_template('contact.html')

@app.route('/new_question', methods=['GET', 'POST'])
@login_required
def add_question():
    print "fo"
    if request.method == 'POST':
        table_url = None
        question = request.form['question']
        row = request.form['row']
        column = request.form['col']
        answer = request.form['answer']
        table_url = request.form['table_url']
        question_obj = Question(current_user.id, question, answer, row, column, table_url)
        db.session.add(question_obj)
        db.session.commit()
        return redirect(url_for('show_question', question_id=question_obj.id))
    return render_template('layout.html')


@app.route('/question/<int:question_id>', methods=['GET'])  #views choosen Question to the user
def show_question(question_id):
    question_obj = Question.query.get(question_id)
    sample_size = 10000000000
    if question_obj:
        header, rows = generate_table(question_obj.table, sample_size)
        return render_template('show_question.html', question_obj=question_obj,
                                header=header, rows=rows, clickable=False,
                                row=question_obj.row, column=question_obj.column)
    flash('This Question does not exist!')
    return render_template('new_question.html')


@app.route('/my-questions', methods=['GET', 'POST'])
@login_required
def my_questions():
    if request.method == 'GET':
        question_objs = Question.query.filter_by(user_id=current_user.id).all()
        return render_template('my_questions.html', question_objs=question_objs)
    if request.method == 'POST':
        if request.form.get("Choosen_Question") is not None:        #choose Question
            question_objs = Question.query.filter_by(user_id=current_user.id).all()
            edit_question = Question.query.get_or_404(request.form.get("Choosen_Question"))
            return render_template('my_questions.html', question_objs=question_objs, edit_question=edit_question)
        if request.form.get("Edit_questionID") is not None:         #edit Question
            edit_question = Question.query.get_or_404(request.form.get("Edit_questionID"))
            change_question(edit_question)
            question_objs = Question.query.filter_by(user_id=current_user.id).all()
            return render_template('my_questions.html', question_objs=question_objs, edit_question=edit_question)
        if request.form.get("deleteThisQuestion") is not None:      #delete Question
            deleteQuestion(request.form.get("deleteThisQuestion"))
            question_objs = Question.query.filter_by(user_id=current_user.id).all()
            return render_template('my_questions.html', question_objs=question_objs)
        if request.form.get("deleteAllQuestions") is not None:      #delete all Questions
            delete_my_Questions()
            return render_template('my_questions.html')
        else: #nothing selected
            flash('Use Checkboxes')
            question_objs = Question.query.filter_by(user_id=current_user.id).all()
            return render_template('my_questions.html', question_objs=question_objs)

def change_question(q):  # check if new entries exist and save changes
    changes = False
    input = request.form.get("question_url")
    if q.table != input:
        q.table = input
        changes = True
    input = request.form.get("question_name")
    if q.question != input:
        q.question = input
        changes = True
    input = request.form.get("question_answer#1")
    if q.answer != input:
        q.answer = input
        changes = True
    input = request.form.get("fake_answer1")
    if q.fake_answer1 != input:
        q.fake_answer1 = input
        changes = True
    input = request.form.get("fake_answer2")
    if q.fake_answer2 != input:
        q.fake_answer2 = input
        changes = True
    input = request.form.get("fake_answer3")
    if q.fake_answer3 != input:
        q.fake_answer3 = input
        changes = True
    input = request.form.get("additional_information")
    #print 'input:', type(input) #print 'add_inf:', type(q.add_info) # input = unicode(input.decode('UTF-8'))
    #unicode(q.add_info.decode('UTF-8'))
    if q.add_info != input:
        q.add_info = input
        changes = True
    if changes == True:
        db.session.commit()
    return

def deleteQuestion(question_id):
    question_obj = Question.query.get_or_404(question_id)
    if current_user != question_obj.user:
        abort(401)
    db.session.delete(question_obj)
    db.session.commit()
    flash('Question deleted')
    return

def delete_my_Questions():
    questions=Question.query.filter(Question.user_id == str(current_user.id)).all()
    for q in questions:
        db.session.delete(q)
    db.session.commit()
    flash('Questions deleted')
    return


@app.route('/quizz', methods=['GET', 'POST'])
@login_required
def new_quizz():
    question_ids = request.form.getlist('questions')
    # store quiz in DB
    quizz_obj = Quizz(current_user.id, request.form.get('quizz_title'))
    db.session.add(quizz_obj)
    # add all questions to the quizz
    for question_id in question_ids:
        question_obj = Question.query.get(question_id)
        if question_obj is not None:
            quizz_obj.questions.append(question_obj)
    db.session.commit()
    flash('Quiz was created successfully! You can view it at "My Quizzes"')
    question_objs = Question.query.filter_by(user_id=current_user.id).all()
    return render_template('my_questions.html', question_objs=question_objs)


@app.route('/my-quizzes', methods=['GET','POST'])
@login_required
def myQuizzes():
    #print 'foooo',current_user
    #if current_user.is_anonymous():
    #    return render_template('my_quizzes.html', quizzes=False)
    #if request.method == 'GET':
    # question_objs = Question.query.filter_by(user=current_user).all()
    # quizz_obj = Quizz(current_user.id, request.form.get('quizz_title'))
    #if current_user.is_authenticated():
    if request.method == 'GET':
        quizzes = Quizz.query.filter_by(user_id=current_user.id).all()
        return render_template('my_quizzes.html', quizzes=quizzes)
    if request.method == 'POST':
        if request.form.get("deleteThisQuizz") is not None:         #delete Quiz
            deleteQuiz(request.form.get("deleteThisQuizz"))
            quizzes = Quizz.query.filter_by(user_id=current_user.id).all()
            return render_template('my_quizzes.html', quizzes=quizzes)
        if request.form.get("deleteAllQuizzes") is not None:        #delete all Quizzes
            delete_my_Quzzes()
            return render_template('my_quizzes.html')
        if request.form.get("changeTitel") is not None:             #edit Titel
            change_q_title(request.form.get("q_id"))
            quizzes = Quizz.query.filter_by(user_id=current_user.id).all()
            return render_template('my_quizzes.html', quizzes=quizzes)
        else:
            flash('Use Checkboxes')
            quizzes = Quizz.query.filter_by(user_id=current_user.id).all()
            return render_template('my_quizzes.html', quizzes=quizzes)

def change_q_title(q_id):  # check if new entries exist and save changes
    q = Quizz.query.get_or_404(q_id)
    changes = False
    title = request.form.get("changeTitel")
    if q.title != title:
        q.title = title
        changes = True
    if changes == True:
        db.session.commit()
    return

def deleteQuiz(q_id):
    q = Quizz.query.get_or_404(q_id)
    if current_user.username != User.query.get_or_404(q.user_id).username:
        abort(401)
    db.session.delete(q)
    db.session.commit()
    flash('Question deleted')
    return

def delete_my_Quzzes():
    quizzes=Quizz.query.filter(Quizz.user_id == str(current_user.id)).all()
    for q in quizzes:
        db.session.delete(q)
    db.session.commit()
    flash('Quizzes deleted')
    return


@app.route('/solve-quiz/<quizz_id>', methods=['GET', 'POST'])
def show_quizz(quizz_id):
    #print "FOOOOOOOOOOquizzID",quizz_id, "request.method", request.method, "request.form", request.form
    if request.method == 'GET':
        quizz_obj = Quizz.query.get_or_404(quizz_id)
        response = make_response(render_template('show_quizz.html', quizz_obj=quizz_obj, answered_question=False))
        response.set_cookie('correct_answersOfUser', '0')  #
        return response
    if request.method == 'POST':
        if request.form.get('user_answer'):
            response = generateResponse()
            return response
        else:   #submitted without providing an answer
            flash(u'Choose a Checkbox!')
            return render_template('show_quizz.html', answered_question=False,
                                   quizz_obj=Quizz.query.get_or_404(request.form.get('quizz_obj.id')),
                                   correct_answers=int(request.cookies.get('correct_answersOfUser')))


def generateResponse():
    quizz_obj = Quizz.query.get_or_404(request.form.get('quizz_obj.id'))
    correct_answers = int(request.cookies.get('correct_answersOfUser'))
    user_answer = request.form.get('user_answer')
    question_obj = Question.query.get_or_404(request.form.get('ques_obj.id'))
    if user_answer == question_obj.answer:
        if correct_answers < len(quizz_obj.questions):#no more correct answers than existing answers
            print(len(quizz_obj.questions))
            correct_answers += 1
    sample_size = 10000000000
    header, rows = generate_table(question_obj.table, sample_size)
    visible_rows = 5
    response = make_response(render_template('show_quizz.html', quizz_obj=quizz_obj, answered_question=question_obj,
                                             user_answer=user_answer, correct_answers=correct_answers,
                                             header=header, rows=rows, clickable=False, visible_rows=visible_rows,
                                             row=question_obj.row, column=question_obj.column))
    response.set_cookie('correct_answersOfUser', str(correct_answers))
    return response

@app.route('/try_out', methods=['POST','GET'])
def open_test():
    print request.form
    if request.form:
        edit_question = request.form['Edit_Question']
        print edit_question
    return render_template('try_out.html')


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.route('/login', methods=['POST','GET'])
def login():
    if request.method == 'POST' and request.form.get('formType') == 'login':
        if current_user.is_authenticated:
            return redirect(url_for('new_question'))
        u_email , u_password = request.form.get('user_email'), request.form.get('user_password1')
        if validateLogin(u_email, u_password):
            u = User.query.filter_by(email=u_email).first()
            login_user(u, remember=False)
            flash('Logged in successfully.')
            return render_template('login.html', user=u)
        else:
            flash('Password or e-Mail wrong')
            return render_template('login.html')
    elif request.method == 'POST' and request.form.get('formType') == 'register':
        result = validateReqistration(request.form.get('user_email'), request.form.get('user_name'),
                                      request.form.get('user_password2'), request.form.get('user_password3'))
        if result[0]:
            u = registerUser()
            return render_template('login.html')
        else:#error
            return render_template('login.html', errors=result.pop(0))
    elif request.method == 'GET':
        return render_template('login.html')
    #Keep in Case of Facebook Log-in
    '''
    return facebook.authorize(callback=url_for('facebook_authorized',
                                               next=request.args.get('next') or request.referrer or None,
                                               _external=True))
    '''

def validateLogin(u_email, u_password):
    result = False
    users = User.query.all()
    for u in users:
        if u.email == u_email:
            if u.check_password(u_password):
                result = True
    return result



def validateReqistration(u_email, u_name, u_password1, u_password2):
    correct = [True]
    if u_password1 != u_password2:
        correct[0]= False
        correct.append("Registration Error1: Password 1 and 2 does not match")
    users = User.query.all()
    for u in users:
        if u.email == u_email:
            correct[0] = False
            correct.append("Registration Error2: Email Adress is already in use")
        if u.username == u_name:
            correct[0] = False
            correct.append("Registration Error3: Username is already in use")
    return correct


def registerUser():
    e_mail, name = request.form.get('user_email'), request.form.get('user_name')
    password1, password2 = request.form.get('user_password2'), request.form.get('user_password3')
    try:
        u = User(username=name, email=e_mail)
        u.set_password(password1)
        db.session.add(u)
        db.session.commit()
        flash('Registration successfull')
        return u
    except Exception as e:
        print "Error: Could not create User"
        error = False
        return error


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('new_question'))
    #Keep in Case of Faceboook Log-Out
    '''
    session.clear()
    flash('You were logged out')
    return redirect(url_for('new_question'))
    '''

'''
@app.route('/login/authorized')
@facebook.authorized_handler
def facebook_authorized(resp):
    next_url = request.args.get('next') or url_for('new_question')
    if resp is None:
        flash('You denied the login')
        return redirect(next_url)

    session['fb_access_token'] = (resp['access_token'], '')

    me = facebook.get('/me')
    user = User.query.filter_by(fb_id=me.data['id']).first()
    if user is None:
        user = User()
        user.fb_id = me.data['id']
        db.session.add(user)

    user.display_name = me.data['name']
    db.session.commit()
    session['user_id'] = user.id

    flash('You are now logged in as %s' % user.display_name)
    return redirect(next_url)


@facebook.tokengetter
def get_facebook_oauth_token():
    return session.get('fb_access_token')
'''



if __name__ == '__main__':
    app.run(port=5001)
