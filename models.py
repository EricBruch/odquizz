from quizz import db
from flask_login import UserMixin
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash

# table with many-to-many relationships for quizzes and questions
# following http://flask-sqlalchemy.pocoo.org/2.1/models/
# and https://techarena51.com/blog/many-to-many-relationships-with-flask-sqlalchemy/
quizz_questions = db.Table('quizz_questions',
                           db.Column('quizz_id', db.Integer, db.ForeignKey('quizz.id'), nullable=False),
                           db.Column('question_id', db.Integer, db.ForeignKey('question.id'), nullable=False),
                           db.PrimaryKeyConstraint('quizz_id', 'question_id'))


class Quizz(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    pub_date = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    questions = db.relationship('Question', secondary=quizz_questions,
                backref=db.backref('quizzes', lazy='dynamic'))

    def __init__(self, user_id, title):
        self.user_id = user_id
        self.title = title
        self.pub_date = datetime.utcnow()
        print "new Quizz object created"


class Question(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    question = db.Column(db.Text)
    answer = db.Column(db.Text)
    fake_answer1 = db.Column(db.Text)
    fake_answer2 = db.Column(db.Text)
    fake_answer3 = db.Column(db.Text)
    add_info = db.Column(db.UnicodeText)
    table = db.Column(db.Text)          #the url of the csv table used
    column = db.Column(db.Integer)      #column of Question from CSV table
    row = db.Column(db.Integer)         #row of Question from CSV table
    pub_date = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __init__(self, user_id, question, answer, row, column, table):
        self.user_id = user_id
        self.question = question
        self.answer = answer
        self.fake_answer1 = "fake answer 1"
        self.fake_answer2 = "fake answer 2"
        self.fake_answer3 = "fake answer 3"
        self.table = table
        self.row = row
        self.column = column
        self.pub_date = datetime.utcnow()
        print "new question object created"


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    fb_id = db.Column(db.String(30), unique=True)
    questions = db.relationship(Question, lazy='dynamic', backref='user')
    reg_date = db.Column(db.DateTime)

    def __init__(self, username, email):
        self.username = username
        self.email = email
        self.reg_date = datetime.utcnow()

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)