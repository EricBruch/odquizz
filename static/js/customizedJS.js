function enable_Edit() {
    "use strict";
    document.getElementById("QuestionURL").disabled = false;
    document.getElementById("QuestionName#1").disabled = false;
    document.getElementById("QuestionAnswer#1").disabled = false;
    document.getElementById("fake_answer#1").disabled = false;
    document.getElementById("fake_answer#2").disabled = false;
    document.getElementById("fake_answer#3").disabled = false;
    document.getElementById("add_information").disabled = false;
    document.getElementById("submit").disabled = false;
}
function showCSVTable() {
    document.getElementById("csv_table1").style.visibility = "visible";
}
(function($) {
  $.fn.shuffle = function() {

    var elements = this.get()
    var copy = [].concat(elements)
    var shuffled = []
    var placeholders = []

    // Shuffle the element array
    while (copy.length) {
      var rand = Math.floor(Math.random() * copy.length)
      var element = copy.splice(rand, 1)[0]
      shuffled.push(element)
    }

    // replace all elements with a plcaceholder
    for (var i = 0; i < elements.length; i++) {
      var placeholder = document.createTextNode('')
      findAndReplace(elements[i], placeholder)
      placeholders.push(placeholder)
    }

    // replace the placeholders with the shuffled elements
    for (var i = 0; i < elements.length; i++) {
      findAndReplace(placeholders[i], shuffled[i])
    }

    return $(shuffled)

  }

  function findAndReplace(find, replace) {
    find.parentNode.replaceChild(replace, find)
  }
})(jQuery);

/*
function shuffle_radioItems() {
    var questions = $("shuffle_radioItems");

    questions.html(
        questions.find("label").sort(function() {
            return Math.round(Math.random()) - 0.5;
        })
    );
}
*/