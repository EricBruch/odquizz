#add init in sperate file
'''
from flask_sqlalchemy import SQLAlchemy

from flask_migrate import Migrate

from flask_login import LoginManager
from config import Config

app = Flask(__name__)
app.config.from_object(Config)  #load configuration secret key & database
db = SQLAlchemy(app)
migrate = Migrate(app, db)
db.create_all()
db.session.commit()

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
'''