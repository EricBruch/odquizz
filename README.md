# Open Data Quizz Builder

Web-site for constructing Open Data (OD) Quizzes.

OD Quizz is a quiz featuring tables with official goverment statistics pulled out from the Open Data portals designed to motivate citizens participation, awareness and transparancy.


Goals: 

(1) provide easy access to and further encorage discovery, exploration and utilization of the goverment statistical data;

(2) crowdsourcing OD annotations for question answering (QA) from tables.


Method: gamification: help of the application to create quizzes semi-automated

## Requirements

* download the application
git clone https://bitbucket.org/EricBruch/odquizz

* download Python

*pip install:

requirements.txt

*install anycsv
follow https://github.com/sebneu/anycsv.git#egg=anycsv part "Install"

*install pyyacp
same as anycsv
path to vitualenv
python setup.py install


## Deployment

For deployment on heroku:
*Procfile: "web: gunicorn quizz:app"
*Heroku account
*Heroku CLI
*go to local path of application
*create heroku application:
heroku apps:create <unique name>
*adding Postgres Database
heroku addons:add heroku-postgresql:hobby-dev
*deloy application
git push heroku master
-> to deploy latest version to heroku


App Domains: localhost
Add Platform -> Website -> Site URL: http://localhost:5000/

* To init and/or clear the DB run restart_db.py

* Start web-app:

python quizz.py


## Acknowledgments

* Jürgen Umbrich, Sebastian Neumaier, Vadim Savenkov. Open Data Hackathon WU. 2017.

* Declarative widgets Polymer elements https://github.com/jupyter-widgets/declarativewidgets/tree/master/elements

* Armin Ronacher. PyLadies Flask Workshop tutorial. https://github.com/mitsuhiko/pyladies-flask

* HTML Quizz Tutorial https://css-tricks.com/building-a-simple-quiz/

## Inspiration and Related Work

* CSV Engine http://data.wu.ac.at/csvengine/clean

* Alan Smith. https://www.ted.com/talks/alan_smith_why_we_re_so_bad_at_statistics

* http://www.neighbourhood.statistics.gov.uk

* Gapminder. Global Ignorance Project. http://www.gapminder.org/ignorance/

* https://www.typeform.com/examples/quizzes/

* https://www.buzzfeed.com/quizzes
