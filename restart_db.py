'''
4 Jul 17
author: svakulenko

DB maintaince script to init and/or clear the DB

'''
from quizz import db

#quizz.db.drop_all()
#quizz.db.create_all()
db.drop_all()
db.create_all()